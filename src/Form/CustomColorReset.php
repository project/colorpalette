<?php

namespace Drupal\colorpalette\Form;

use Drupal\colorpalette\ColorPaletteUtility;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides confirmation form for resetting a vocabulary to colorwise order.
 *
 * @internal
 */
class CustomColorReset extends ConfirmFormBase {

  /**
   * The color palette utility.
   *
   * @var \Drupal\colorpalette\ColorPaletteUtility
   */
  protected $colorPalette;

  /**
   * Constructs a new CustomColorReset.
   *
   * @param \Drupal\colorpalette\ColorPaletteUtility $colorpalette_utility
   *   The color palette utility.
   */
  public function __construct(ColorPaletteUtility $colorpalette_utility) {
    $this->colorPalette = $colorpalette_utility;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('colorpalette.utility')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'taxonomy_vocabulary_confirm_reset_colorwise';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to reset the vocabulary Colors to colorwise?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('entity.taxonomy_vocabulary.overview_form', ['taxonomy_vocabulary' => 'colorpalette_colors']);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Resetting a vocabulary will discard all custom ordering and sort items colorwise.');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Reset to colorwise');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->colorPalette->resetColorWise();
    $this->messenger()->addStatus($this->t('Reset vocabulary Colors to Colorwise.'));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
