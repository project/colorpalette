# Color Palette

Color Palette module provides a `Field Widget` that helps in launching a color palette with a pre-approved color options for capturing entity field inputs.
Unlike any other colorpicker, this widget only shows configured set of
color(managed over a taxonomy) options in the palette. Further, color palette
could be configured per field level to filter palette color options with
say, Light colors only, Dark colors only, both Light & Dark colors etc.

Color palette widget collect color input value in hexcode for entity fields.
Example value, #ff0000, #00ff00 etc. This widget supports `Text` and
`Taxonomy Term` field type at the moment.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/colorpalette).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/colorpalette).


## Table of contents

- Requirements
- Installation
- Permissions
- Configuration


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).



## Permissions

Find `Permission` link against the Color Palette module on Extend page(i.e., at *admin/modules*), OR direct link *admin/people/permissions#module-colorpalette*.

Available permission(s):
1. `Administer Palette`: User with this permission can create new color(s) on-the-fly or re-use existing color(s) from the launched palette itself.


## Configuration

### New Taxonomy/Vocabulary

Once installed this module provides two new taxonomy/vocabulary:
1. `Colors`: New colors can be introduced creating terms within this
vocabulary. Direct link-
*admin/structure/taxonomy/manage/colorpalette_filter_tags/overview*
1. `Color Filter Tags`: Terms of this vocabulary could be used to filter colors options in the palette. Example, Light, Dark etc. Direct link-
*admin/structure/taxonomy/manage/colorpalette_filter_tags/overview*

Now, any of the entity field of type `Text` OR `Taxonomy Term` could leverage
the color palette input options while creating respective contents. Only
published colors are used in the color palette.

### Field Settings
To use Color Palette widget for node entity field, perform below actions:
1. Navigate to `Structure > Content Types > My Node Type > Manage Form Display`
  against the concerned node type
1. Opt for `Color Palette` widget against the respective field(s)
1. Click on respective gear-icon for settings now choose `Filter tags` (This step is optional though)
1. Finally, save the page
1. Now, navigate to `Structure > Content Types > My Node Type > Manage Fields` and make sure vocabulary `Color` is selected for the respective field(s) settings

Now, while creating the node, the author will find that a color palette launches
for the respective fields. Similar steps can be followed for configuring other entity fields as well say, user entity.

### Color Palette Interaction
- Selecting/Deselecting colors
   1. Click on any of the color to use color's hexcode value for the field
   1. To deselect or clear the field value use `Clear` button
- Create New Color (`Administer Palette` permission is required to perform this action)
   1. Click on `New Color` button (To pop `Add New Color` form)
   1. Choose a `Color`
   1. Provide `Name`
   1. Add `Filter Tags`
   1. Click on `Submit & Apply` button

**Important:** While creating a new color by using `New Color` button, please remember that
1. When user input for `Color` field DOES NOT exists, then
   - A new Color will be created
   - User input for `Filter Tags` field will be merged with the `Filter tags` settings configured while opting the field widget
   - Finally, this new color created as a term in the `Colors` vocabulary will have the above merged `Filter Tags` value
1. When user input for `Color` field DO exists, then
   - A new Color will NOT be created
   - Exiting color will be used
   - Existing color if NOT published will get published forcefully
   - User input for `Filter Tags` field will be merged with the `Filter tags` settings configured while opting the field widget and `Filter Tags` value of exiting color
   - Finally, this exiting color term in the `Colors` vocabulary will have the above merged `Filter Tags` value
